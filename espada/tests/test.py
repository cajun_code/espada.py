import click
from click.testing import CliRunner
import os
import unittest

import espada.main as esp

class TestEspadaCommandLine(unittest.TestCase):
    def test_workspace_create(self):
        """ As a developer when I run this command I should get a workspace project generated """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'ws', 'huntWampas'])
            assert result.exit_code == 0
            assert "Creating Library Project: libHuntWampas" in result.output 
            assert "Creating Test Project: testsHuntWampas" in result.output
            assert "Creating Application Project: HuntWampas" in result.output
    
    def test_workspace_Lib_Loaded(self):
        """ As a developer when I run this command I should libraries installed in my test project """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'ws', 'huntWampas'])
            assert result.exit_code == 0
            assert "Creating Library Project: libHuntWampas" in result.output 
            assert "Creating Test Project: testsHuntWampas" in result.output
            assert "Fetching file include/catch.hpp" in result.output
            assert "Fetching file include/fakeit.hpp" in result.output
            # assert os.path.isfile("HuntWampas/testsHuntWampas/include/catch.hpp")
            # assert os.path.isfile("HuntWampas/testsHuntWampas/include/fakeit.hpp")
            assert "Creating Application Project: HuntWampas" in result.output

    def test_app_create(self):
        """ As a developer when i run this command I should get a single app project with a cmake file and main cpp """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'app', 'HelloWorld'])
            assert result.exit_code == 0
            assert os.path.isfile("HelloWorld/CmakeLists.txt")
            assert os.path.isfile("HelloWorld/src/main.cpp")
    
    def test_lib_create(self):
        """ As a developer when i run this command I should get a single Library project with a cmake file and main cpp """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'lib', 'DatRest'])
            assert result.exit_code == 0
            #assert "Creating Library Project: libDatRest" in result.output
            assert os.path.isfile("libDatRest/CmakeLists.txt")
            
class TestEspadaClassGenerator(unittest.TestCase):
    def test_create_class(self):
        """ As a developer when i need to gennerate a class I should get a header file and source file """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'lib', 'DatRest'])
            assert result.exit_code == 0
            os.chdir("libDatRest")
            result = runner.invoke(esp.class_gen, ['--test', '-c', 'RestClient'])
            assert result.exit_code == 0
            assert os.path.isfile("include/rest_client.h")
            assert os.path.isfile("src/rest_client.cpp")

    def test_create_class_in_workspace(self):
        """ As a developer when i need to gennerate a class I should get a header file and source file """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'ws', 'huntWampas'])
            assert result.exit_code == 0
            os.chdir("libHuntWampas")
            result = runner.invoke(esp.class_gen, ['--test', '-c', 'Cave'])
            #print result.output
            assert result.exit_code == 0
            assert os.path.isfile("include/cave.h")
            assert os.path.isfile("src/cave.cpp")
            assert os.path.isfile("../testsHuntWampas/src/cave_tests.cpp")

class TestEspadaFileGenerator(unittest.TestCase):
    def test_create_header_in_lib(self):
        """ As a developer when i need to gennerate a header file i should get a file in the include directory """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'lib', 'DatRest'])
            assert result.exit_code == 0
            os.chdir("libDatRest")
            result = runner.invoke(esp.header_gen, ['-f', 'Constants', "-p", "."])
            #print result.output
            assert result.exit_code == 0
            assert os.path.isfile("include/constants.h")
    
    def test_create_Source_in_lib(self):
        """ As a developer when i need to gennerate a source file i should get a file in the src directory """
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(esp.project, ['-t', 'lib', 'DatRest'])
            assert result.exit_code == 0
            os.chdir("libDatRest")
            result = runner.invoke(esp.source_gen, ['-f', 'Constants', "-p", "."])
            #print result.output
            assert result.exit_code == 0
            assert os.path.isfile("src/constants.cpp")

if __name__ == '__main__':
    unittest.main()