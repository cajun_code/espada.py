

SET( INCLUDE_DIRS
	${CMAKE_CURRENT_LIST_DIR}/include
)
include_directories( ${INCLUDE_DIRS} )
include_directories(${LIB{{project_name}}_INCLUDE_DIRS})
file(GLOB SOURCES "src/*.cpp")

add_executable(test_{{exe_name}} ${SOURCES})
target_link_libraries(test_{{exe_name}} lib{{project_name}})

add_test(test_{{exe_name}} test_{{exe_name}} -s)
