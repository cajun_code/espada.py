// {{f_name}}.h create by {{user_name}} on {{date_time}}
#pragma once

class {{class_name}}{
public:
    {{class_name}}();
    virtual ~{{class_name}}();

};