Usage of Erebus
===============

Creating a Project
------------------

..code::
    erebus project PROJECT_NAME



Creating a Class
----------------

..code::
    erebus class -c player --test


Create a Test Case
------------------

..code::
    erebus test -c name


Create a Header File
--------------------

..code::
    erebus header -l app -f name

Create a Source File
--------------------

..code::
    erebus source -l app -f name
